# Sockets

## References

* [http://socketo.me/](http://socketo.me/)
* [https://stackoverflow.com/questions/38477346/connecting-to-a-ratchet-websocket-server-using-php](https://stackoverflow.com/questions/38477346/connecting-to-a-ratchet-websocket-server-using-php)


## Run

`$ php server.php`


## Server

The file `server.php` creates and starts a server on its own IP and port 8080.
This server should be running to allow clients to get and send messages.


## Connect from JS clients (client.html)

```javascript
var conn = new WebSocket('ws://192.56.1.30:8080');

conn.onopen = function(e) {
    console.log("Connection established!");
};

conn.onmessage = function(e) {
    console.log(e.data);
};
```


### Send message from JS client

```javascript
conn.send('The message here');
```


## Connect from PHP clients (client.php)

Install *Texttalk\websocket* with `composer require textalk/websocket 1.0.*` then run `composer dump-autoload`


```php
require('vendor/autoload.php');
use WebSocket\Client;

$client = new Client("ws://192.56.1.30:8080");
```


### Send message from PHP client

```php
$client->send("Message from PHP client here");
```

The client should return `true` (or `false`) to avoid errors on console and block further emits.